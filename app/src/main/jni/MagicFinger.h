//
// Created by JanHan on 16/3/19.
//

#ifndef MAGICFINGER_MAGICFINGER_H
#define MAGICFINGER_MAGICFINGER_H

#include <easyar/matrix.hpp>
#include "renderer.hpp"
#include "ar.hpp"

namespace EasyAR {
    namespace magicfinger {

        class MagicFinger : public AR {
        public:
            MagicFinger();

            ~MagicFinger();

            virtual void initGL();

            virtual void resizeGL(int width, int height);

            virtual void render();

            virtual bool clear();

            void recognizedPic(int i);

        private:

            static const int image_number = 6;

            Vec2I view_size;
            VideoRenderer *renderer[image_number];
            int tracked_target;
            int active_target;
            int texid[image_number];

            std::string pic_name[6] = {"father", "plane", "noname19", "noname20",
                        "noname29", "noname36"};
            std::string video_name[6] = {"father.mp4", "plane.mp4", "noname19.mp4", "noname20.mp4",
                                       "noname29.mp4", "noname36.mp4"};

            ARVideo *video;
            VideoRenderer *video_renderer;
        };
    }
}


#endif //MAGICFINGER_MAGICFINGER_H
