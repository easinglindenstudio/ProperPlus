package yixingren.magicfinger;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

/**
 * Created by JanHan on 16/3/19.
 *
 */
public class AuthorInfoActivity extends Activity{

    private WebView webView;
    private int trackedPicId = -1;
    private int authorId = 0;

    private final String LOG_TAG = "AuthorInfoActivity";
    private String[] authorUrl = {
            "http://r.xiumi.us/stage/v3/29lbI/11253698",
            "http://r.xiumi.us/stage/v3/2tWUH/11269634"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorinfo);

        trackedPicId = getIntent().getIntExtra(CommonVariables.TRACKED_PIC_ID_SRING, -1);
        if (trackedPicId > -1) {

            Log.d(LOG_TAG, "trackedPicId: " + trackedPicId);
            switch (trackedPicId) {
                case 1:
                case 2:
                    authorId = 0;
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                    authorId = 1;
                    break;
            }
            initWebView();
        } else {

            Toast.makeText(this, "找不到作者信息", Toast.LENGTH_LONG).show();
        }
    }

    private void initWebView() {

        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        WebSettings webSettings = webView.getSettings();
        webSettings.setAllowFileAccess(true);
        webSettings.setBuiltInZoomControls(true);
        webView.loadUrl(authorUrl[authorId]);

        //加载数据
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
//                    BonusActivity.this.setTitle("加载完成");
                } else {
//                    BonusActivity.this.setTitle("加载中.......");
                }
            }
        });

        //这个是当网页上的连接被点击的时候
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(final WebView view,
                                                    final String url) {
                webView.loadUrl( url);
                return true;
            }
        });
    }

    public void backBtnClicked( View view) {

        this.finish();
    }

    public boolean onKeyDown(int keyCoder, KeyEvent event) {
        if (webView.canGoBack()) {

            if (keyCoder == KeyEvent.KEYCODE_BACK) {

                webView.goBack();
                return true;
            }
        } else {
            this.finish();
        }
        return false;
    }
}
