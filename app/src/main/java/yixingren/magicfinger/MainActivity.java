/**
* Copyright (c) 2015-2016 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
* EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
* and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
*/

package yixingren.magicfinger;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import cn.easyar.engine.EasyAR;
import yixingren.bonus.BonusActivity;

public class MainActivity extends Activity{

    static String key = "a7cbcecf2ef5f63e5b788b577c814edfBGEZ5EjiIV41a9jcIpVYRTCvZUZqml" +
            "LO4BBuLgfBUy7mvKwblgPIyc3RhhXEmWDgC2hnpqQJmjPd7BhDCeXeEYudwYNOS6sNkuBkqrc2" +
            "t3YVTlAS51BkN0DNIcI4akabJ5UeaEPgasPZhQinXUBJdHQlSJa8mrleC3jBANl0";

    static {
        System.loadLibrary("EasyAR");
        System.loadLibrary("MagicFinger");
    }

    public static native void nativeInitGL();
    public static native void nativeResizeGL(int w, int h);
    public static native void nativeRender();
    private native boolean nativeInit();
    private native void nativeDestory();
    private native void nativeRotationChange(boolean portrait);

    private static ImageButton authorInfoBtn, bonusBtn, tipBtn;
    private static Activity activity;
    private static int trackedPicId = -1;

    private static final String LOG_TAG = "BonusActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        activity = this;
        bindView();

        EasyAR.initialize(this, key);
        nativeInit();

        GLView glView = new GLView(this);
        glView.setRenderer(new Renderer());
        glView.setZOrderMediaOverlay(true);

        ((ViewGroup) findViewById(R.id.preview)).addView(glView,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        nativeRotationChange(getWindowManager().getDefaultDisplay().getRotation() == android.view.Surface.ROTATION_0);

    }

    private void bindView() {

        authorInfoBtn = (ImageButton) findViewById(R.id.author_info);
        bonusBtn = (ImageButton) findViewById(R.id.bonus);
        tipBtn = (ImageButton) findViewById(R.id.tip_btn);
    }

    private static void setBtnVisibility(int visibility) {

        authorInfoBtn.setVisibility(visibility);
        bonusBtn.setVisibility(visibility);
    }

    public void authorBtnClicked(View view) {

        Log.d(LOG_TAG, "authorClick");
        Intent intent = new Intent(this, AuthorInfoActivity.class);
        intent.putExtra(CommonVariables.TRACKED_PIC_ID_SRING, trackedPicId);
        startActivity(intent);
    }

    public void bonusBtnClicked(View view) {

        Log.d(LOG_TAG, "bonusClick");
        Intent intent = new Intent(this, BonusActivity.class);
        intent.putExtra(CommonVariables.TRACKED_PIC_ID_SRING, trackedPicId);
        startActivity(intent);
    }

    //c++ invoke the method below
    public static void recognizedPic(final int i) {

        trackedPicId = i;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (i == -1) {

                    setBtnVisibility(View.GONE);
                    tipBtn.setVisibility(View.VISIBLE);
                } else {
                    setBtnVisibility(View.VISIBLE);
                    tipBtn.setVisibility(View.GONE);
                }
            }
        });
//        Log.d(LOG_TAG, "recognizedPic " + i);
    }

    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
        nativeRotationChange(getWindowManager().getDefaultDisplay().getRotation() == android.view.Surface.ROTATION_0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        nativeDestory();
        Log.d(LOG_TAG, "onDestroy");
    }

    @Override
    protected void onResume() {
        super.onResume();
        EasyAR.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EasyAR.onPause();
    }

    private long exitTime;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {

            if((System.currentTimeMillis()-exitTime) > 2000){

                Toast.makeText(getApplicationContext(), "再按一次退出！", Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {

                finish();
                System.exit( 0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
