package yixingren.bonus;

import java.io.IOException;
import android.media.MediaRecorder;
import android.util.Log;

public class Microphone {

	private MediaRecorder recorder;
	private int level;
	
	public Microphone(){
        // Audio recorder
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile("/dev/null");
       try {
			recorder.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        recorder.start();
     
	}

	public void releaseRecorder() {

		recorder.stop();
//		recorder.release();
	}

	//这里是检测音量大小的算法。。。没有统计时长可以尝试加个循环
	public int getLevel(){

		level = recorder.getMaxAmplitude();
		Log.d("BonusActivity", "micro level " + level);
		level = (level/2000) % 10;
		return level;
	}
}
