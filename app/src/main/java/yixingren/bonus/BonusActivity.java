package yixingren.bonus;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.IOException;
import java.io.InputStream;

import yixingren.magicfinger.CommonVariables;
import yixingren.magicfinger.R;

public class BonusActivity extends Activity {

    private static Activity app;
    private GuaGuaKa guaGuaKa;
    private RelativeLayout rootView;
    private ImageButton blowTipBtn;
    private ImageView resultBtn, picBtn, fakeFogBtn;

    private BlowListener blowListener;

    private final String LOG_TAG = "BonusActivity";
    private String picArray[] = {"father.jpg", "plane.jpg", "noname19.jpg", "noname20.jpg",
            "noname29.jpg", "noname36.jpg"};

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bonus);

        app = this;

        int trackedPicId = getIntent().getIntExtra(CommonVariables.TRACKED_PIC_ID_SRING, -1);
		if ((trackedPicId > -1)&&(trackedPicId<7)){
			Log.e("TAG", "trackedPicId: " + trackedPicId);
			CommonVariables.trackedId = trackedPicId;
		}

        initView();
	}

    private void initView() {

        fakeFogBtn = (ImageView) findViewById(R.id.fake_fog);
        blowTipBtn = (ImageButton) findViewById(R.id.tip_blow);

        picBtn = (ImageView) findViewById(R.id.pic_btn);
        picBtn.setImageBitmap(getImageFromAssetsFile(picArray[CommonVariables.trackedId - 1]));

        resultBtn = (ImageView) findViewById(R.id.result_pic);

        rootView = (RelativeLayout) findViewById(R.id.bonus_root_view);
        guaGuaKa = new GuaGuaKa(this);
        guaGuaKa.setOnWippedListener(new GuaGuaKa.OnWipedListener() {
            @Override
            public void wiped() {

                app.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        resultBtn.setVisibility(View.VISIBLE);
                        resultBtn.setImageResource(R.drawable.got_nothing);
                    }
                });
            }
        });
        rootView.addView(guaGuaKa);

        blowListener = new BlowListener(new BlowListener.OnBlewListener() {
            @Override
            public void blew() {

                fakeFogBtn.setVisibility(View.GONE);
                blowTipBtn.setVisibility(View.GONE);
                guaGuaKa.setBlew();
            }
        });
    }

    private Bitmap getImageFromAssetsFile(String fileName) {

        Bitmap image = null;
        AssetManager am = getResources().getAssets();
        try {

            InputStream is = am.open(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return image;
    }

    protected void onStop() {

        blowListener.releaseResource();
        super.onStop();
    }
}
